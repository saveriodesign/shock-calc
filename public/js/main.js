/** Establish a damage distribution line between two points */
class Line {

    constructor ( x1, x2, y1, y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
        this.m = ( y2 - y1 ) / ( x2 - x1 );
        this.b = y1 - this.m * x1;
    }

    /** Get y at a given x */
    getY ( x ) {
        return this.b + this.m * x;
    }

    /** Get x for a given y */
    getX ( y ) {
        return ( y - this.b ) / this.m;
    }

    /**
     * Get the definite integral of the line between two values of x,
     * this represents the probability the next hit will be in the
     * given range
     */
    integrate ( start, end ) {

        start = start? start : this.x1;
        end = end? end : this.x2;

        return ( this.b * end + this.m * end * end / 2 ) - ( this.b * start + this.m * start * start / 2 );

    }

}

/**
 * Combine an array of Lines which may intersect into
 * a new array of Lines which do not intersect
 */
function combineLines ( ...lines ) {

    const sortedLines = lines.sort(( a, b ) => a.x1 - b.x1 );
    const l = sortedLines.length;
    let overlap = 0;

    return sortedLines.reduce(( stepwise, line, i, array ) => {

        let start = line.x1;

        if ( overlap ) {

            /** Add up the overlapping functions to get sum of y */
            y = ( x ) => {
                let ans = 0;
                for ( let j = 0; j <= overlap; j ++ ) {
                    ans += array[ i - j ].getY( x );
                }
                return ans;
            }

            // have overlap, oldest overlap ends before another begins
            if ( i + 1 >= l || array[ i - overlap ].x2 < array[ i + 1 ].x1 ) {

                // loop while the current overlap is ending before another begins
                while ( overlap && ( i + 1 >= l || array[ i - overlap ].x2 < array[ i + 1 ].x1 )) {

                    // push step
                    stepwise.push( new Line(
                        start,
                        array[ i - overlap ].x2,
                        y( start ),
                        y( array[ i - overlap ].x2 )
                    ));
                    start = array[ i - overlap ].x2;
                    overlap --;

                }

            }
    
            // still have overlap, another overlap starts
            if ( overlap && i + 1 < l && array[ i - overlap ].x2 > array[ i + 1 ].x1 ) {

                // push step
                stepwise.push( new Line(
                    start,
                    array[ i + 1 ].x1,
                    y( start ),
                    y( array[ i + 1 ].x1 )
                ));
                overlap ++;
                return stepwise

            }

        }

        // no overlap and any line but the first
        // add a flat zero line between regions
        else if ( !overlap && i ) stepwise.push(new Line( array[ i - 1 ].x2, line.x1, 0, 0 ));
        
        // an overlap starts
        if ( i + 1 < l && line.x2 > array[ i + 1 ].x1 ) {

            // push step
            stepwise.push( new Line(
                start,
                array[ i + 1 ].x1,
                line.getY( start ),
                line.getY( array[ i + 1 ].x1 )
            ));
            overlap ++;

        }

        // no overlap
        else {

            stepwise.push( new Line(
                start,
                line.x2,
                line.getY( start ),
                line.y2
            ));

        }

        return stepwise;

    }, []);

}

/** Get the damage amplifier of lightning arrow */
function lightningArrowAmplifier ( level ) {
    return ( level )? 1.95 + 0.1 * level : 1;
}

function getEverything ( min, max, speed, acc, crit, multi, double, lucky, autoCrit, chance, window, amplifier, modifier, cap, threshold ) {

    let data = {
        distribution: [],
        minDamage: min,
        maxDamage: max,
        c95damage: null,
        c95probability: null,
        sustainedShock: null
    };

    // establish upper bound
    if ( crit ) data.maxDamage *= multi;
    if ( double ) data.maxDamage *= 2;

    // establish lines
    const range = max - min + 1;
    let hitLine;
    let critLine;
    let doubleHitLine;
    let doubleCritLine;

    const hitOdds = chance * acc * ( 1 - crit ) * ( 1 - double ) / range;
    const critOdds = ( autoCrit? 1 : chance ) * acc * crit * ( 1 - double ) / range / multi;
    const doubleHitOdds = chance * acc * ( 1 - crit ) * double / range / 2;
    const doubleCritOdds = ( autoCrit? 1 : chance ) * acc * crit * double / range / multi / 2;

    if ( lucky ) {

        hitLine = new Line( min, max, Math.pow( hitOdds, 2 ), 1 - Math.pow( 1 - hitOdds, 2 ));
        if ( crit ) critLine = new Line( min * multi, max * multi, Math.pow( critOdds, 2 ), 1 - Math.pow( 1 - critOdds, 2 ));
        if ( double ) doubleHitLine = new Line( min * 2, max * 2, Math.pow( doubleHitOdds, 2 ), 1 - Math.pow( 1 - doubleHitOdds, 2 ));
        if ( double && crit ) doubleCritLine = new Line( min * multi * 2, max * multi * 2, Math.pow( doubleCritOdds, 2 ), 1 - Math.pow( 1 - doubleCritOdds, 2 ));
        
    }
    else {

        hitLine = new Line( min, max, hitOdds, hitOdds );
        if ( crit ) critLine = new Line( min * multi, max * multi, critOdds, critOdds );
        if ( double ) doubleHitLine = new Line( min * 2, max * 2, doubleHitOdds, doubleHitOdds );
        if ( double && crit ) doubleCritLine = new Line( min * multi * 2, max * multi * 2, doubleCritOdds, doubleCritOdds );

    }

    // combine linear functions
    let lineArray = [ hitLine ];
    if ( crit ) lineArray.push( critLine );
    if ( double ) lineArray.push( doubleHitLine );
    if ( crit && double ) lineArray.push( doubleCritLine );

    let stepwiseDistribution = combineLines( ...lineArray );


    // resolve data points for rendering
    data.distribution = getDistribution( ...stepwiseDistribution );

    // integrate the stepwise function to find where the confidence 95 hit resides
    const l = stepwiseDistribution.length;
    let c95step;
    let probabilitySum = 0;
    let i = l - 1;

    while ( !c95step ) {

        // catch failure to apply shock
        if ( i < 0 ) {
            data.sustainedShock = 0;
            return data;
        }
        const stepArea = stepwiseDistribution[ i ].integrate();
        probabilitySum += stepArea;
        if ( Math.pow(( 1 - probabilitySum ), speed * window ) < 0.05 ) {
            c95step = stepwiseDistribution[ i ];
            probabilitySum -= stepArea;
        }
        i--;

    }



    // Newton's method
    let highGuess = c95step.x2;
    let lowGuess = c95step.x1;
    let counter = 0;
    while ( !data.c95damage ) {
        counter ++;
        let guess = ( highGuess + lowGuess ) / 2;
        let diff = Math.pow(( 1 - probabilitySum - c95step.integrate( guess, c95step.x2 )), speed * window ) - 0.05;
        if ( Math.abs( diff ) < 0.001 ) data.c95damage = Math.round( guess );
        else if ( diff > 0 ) highGuess = guess;
        else lowGuess = guess;
    }


    data.sustainedShock = getSustainedShock( data.c95damage * amplifier, threshold, modifier, cap );
    
    // find the height of the confidence 95 probability line
    data.c95probability = c95step.getY( data.c95damage );

    return data;

}

function getDistribution ( ...lines ) {
    return lines.reduce(( points, step ) => {
        points.push(
            {
                damage: step.x1,
                probability: step.y1
            },
            {
                damage: step.x2,
                probability: step.y2
            }
        );
        return points;
    }, []);
}

/**
 * Get the shock applied by a single hit
 * @param {number} hit Damage applied
 * @param {number} threshold Monster ailment threshold
 * @param {number} modifier Added effect of shock
 * @param {number} cap Shock cap
 */
function getShock ( hit, threshold, modifier, cap ) {
    let shock = Math.pow( hit / threshold, 0.4 ) * ( 1 + modifier ) / 2;
    if ( shock < 0.05 ) return 0;
    if ( shock > cap ) return cap;
    return Math.floor( shock * 100 ) / 100;
}

/**
 * Get the max sustainable shock
 * @param {number} hit Damage applied
 * @param {number} threshold Monster ailment threshold
 * @param {number} modifier Added effect of shock
 * @param {number} cap Shock cap
 */
function getSustainedShock ( hit, threshold, modifier, cap ) {
    let oldShock = 0;
    let newShock = getShock( hit, threshold, modifier, cap );
    while ( oldShock !== newShock ) {
        oldShock = newShock;
        newShock = getShock( hit * ( 1 + oldShock ), threshold, modifier, cap );
    }
    return newShock;
}

/**
 * Get the value of mean hit
 * @param {number} min 
 * @param {number} max 
 * @param {number} crit 
 * @param {number} multi 
 * @param {number} double 
 * @param {number} shock 
 */
function getAverageHit ( min, max, crit, multi, double, shock ) {
    const avgHit = ( min + max ) / 2;
    return avgHit * ( 1 - crit - double + multi * crit * ( 1 - double ) + 2 * double * ( 1 - crit ) + 2 * multi * crit * double ) * ( 1 + shock );
    //                |---    Hit   --|   |--          Crit         --|   |--      Double       --|   |--    Double Crit    --|     |-- Shock --|
}

/* Chart constants */

const margin = {
    top: 40,
    right: 40,
    bottom: 40,
    left: 40
};
const w = 710 - margin.left - margin.right;
const h = 420 - margin.top - margin.bottom;

/* Interactivity */

document.addEventListener( 'DOMContentLoaded', ( e ) => {

    /** User Inputs */

    const threshold = d3.select( '#ailment-threshold' );
    const minHit = d3.select( '#min-hit' );
    const maxHit = d3.select( '#max-hit' );
    const atkSpeed = d3.select( '#attack-speed' );
    const crit = d3.select( '#crit-chance' );
    const multi = d3.select( '#crit-multiplier' );
    const shockMod = d3.select( '#shock-modifier' );
    const overshock = d3.select( '#overshock' );
    const lucky = d3.select( '#lucky' );
    const voltaxic = d3.select( '#voltaxic' );
    const arrow = d3.select( '#lightning-arrow' );
    const arrowLevel = d3.select( '#lightning-arrow-level' );
    const shockValueResult = d3.select( '#shock-value' );
    const effectiveDps = d3.select( '#effective-dps' );
    const shockDuration = d3.select( '#shock-duration-modifier' );
    const painseeker = d3.select( '#painseeker' );
    const painseekerRoll = d3.select( '#painseeker-roll' );
    const shockChance = d3.select( '#shock-chance' );
    const doubleDamage = d3.select( '#double-damage' );
    const accuracy = d3.select( '#accuracy' );

    /** Toggled input labels */
    const arrowLevelLabel = d3.select( '#lightning-arrow-level-label' );
    const painseekerRollLabel = d3.selectAll( '.painseeker-roll-label' );

    /** Distribution Chart */

    const x = d3.scaleLinear()
        .domain([ 0, 900 ])
        .range([ 0, w ]);

    const y = d3.scaleLinear()
        .domain([ 0, 1 ])
        .range([ h, 0 ]);

    const xAxisCall = d3.axisBottom( x ).tickFormat( d3.format( '.2s' ));

    const yAxisCall = d3.axisLeft( y ).tickValues([])//.tickFormat(( d, n ) => '' );

    const chartWrapper = d3.select( '#hit-distribution' );

    const distSvg = chartWrapper.append( 'svg' )
        .attr( 'width', w + margin.left + margin.right )
        .attr( 'height', h + margin.top + margin.bottom )
        .append( 'g' )
        .attr( 'transform', `translate(${ margin.left },${ margin.top })` );
    
    const chartTitle = distSvg.append( 'text' )
        .attr( 'transform', `translate(${ w / 2 },0)` )
        .attr( 'font-family', 'Arial, Helvetica, sans-serif' )
        .attr( 'font-size', `${ 3 * margin.top / 4 }px` )
        .attr( 'font-weight', 'bold' )
        .style( 'text-anchor', 'middle' )
        .text( 'Shock Application Distribution' );

    const xAxis = distSvg.append( 'g' )
        .attr( 'id', 'x-axis' )
        .classed( 'axis', true )
        .style( 'font-size', '0.7em' )
        .style( 'font-family', 'sans-serif' )      
        .attr( 'transform', `translate(0,${ h })` )
        .call( xAxisCall );

    const yAxis = distSvg.append( 'g' )
        .attr( 'id', 'y-axis' )
        .classed( 'axis', true )
        .style( 'font-size', '0.7em' )
        .style( 'font-family', 'sans-serif' )      
        .call( yAxisCall );

    const xAxisLabel = distSvg.append( 'text' )
        .attr( 'transform', `translate(${ w / 2 },${ h + 6 * margin.bottom / 7 })` )
        .attr( 'font-family', 'Arial, Helvetica, sans-serif' )
        .style( 'text-anchor', 'middle' )
        .text( 'Damage at 0% Shock' );

    const yAxisLabel = distSvg.append( 'text' )
        .attr( 'transform', `rotate(-90)translate(${ - h / 2 },${ - 2 * margin.left / 3 })` )
        .attr( 'font-family', 'Arial, Helvetica, sans-serif' )
        .style( 'text-anchor', 'middle' )
        .text( 'Relative Freqency' );
    
    const confidenceLine = distSvg.append( 'path' )
        .attr( 'id', 'confidence-line' )
        .classed( 'line', true );
    
    const distLine = distSvg.append( 'path' )
        .attr( 'id', 'distribution-line' )
        .classed( 'line', true );

    const confidenceLabel = distSvg.append( 'text' )
        .classed( 'hide', true )
        .attr( 'font-family', 'Arial, Helvetica, sans-serif' )
        .style( 'text-anchor', 'middle' );

    const lineCall = d3.line();

    arrow.on( 'change', () => {
        if ( !!arrow.property( 'checked' )) {
            lucky.property( 'checked', false );
            arrowLevel.classed( 'hide', false );
            arrowLevelLabel.classed( 'hide', false );
        }
        else {
            arrowLevel.classed( 'hide', true );
            arrowLevelLabel.classed( 'hide', true );
        }
    });

    painseeker.on( 'change', () => {
        if ( !!painseeker.property( 'checked' )) {
            painseekerRoll.classed( 'hide', false );
            painseekerRollLabel.classed( 'hide', false );
        }
        else {
            painseekerRoll.classed( 'hide', true );
            painseekerRollLabel.classed( 'hide', true );
        }
    });

    lucky.on( 'change', () => {
        if (!!lucky.property( 'checked' )) {
            arrow.property( 'checked', false );
            arrowLevel.classed( 'hide', true );
            arrowLevelLabel.classed( 'hide', true );
        }
    });

    d3.select( '#calculate' ).on( 'click', () => {

        const minHitValue = Number( minHit.property( 'value' ));
        const maxHitValue = Number( maxHit.property( 'value' ));
        const atkSpeedValue = Number( atkSpeed.property( 'value' ));
        const critValue = Number( crit.property( 'value' )) / 100;
        const multiValue = Number( multi.property( 'value' )) / 100;
        const shockModValue = Number( shockMod.property( 'value' )) / 100;
        const thresholdValue = Number( threshold.property( 'value' )) * 1e6;
        const arrowLevelValue = Number( arrowLevel.property( 'value' ));
        const shockDurationValue = 2 + Number( shockDuration.property( 'value' )) / 50;
        const painseekerRollValue = Number( painseekerRoll.property( 'value' )) / 100;
        const shockChanceValue = Number( shockChance.property( 'value' )) / 100;
        const doubleDamageValue = Number( doubleDamage.property( 'value' )) / 100;
        const accuracyValue = Number( accuracy.property( 'value' )) / 100;

        if (
            !minHitValue ||
            !maxHitValue ||
            !atkSpeedValue ||
            !thresholdValue ||
            !multiValue
        ) return;        

        // Calculate hit distribution
        const amplifier =
            1 +
            ( !!voltaxic.property( 'checked' )? 3 : 0 ) +
            ( !!arrow.property( 'checked' )? lightningArrowAmplifier( arrowLevelValue ) : 0 ) +
            ( !!painseeker.property( 'checked' )? painseekerRollValue : 0 );
        const shockCap = !!voltaxic.property( 'checked' )? 1 : !!overshock.property( 'checked' )? 0.6 : 0.5;
        const data = getEverything(
            minHitValue,
            maxHitValue,
            atkSpeedValue,
            accuracyValue,
            critValue,
            multiValue,
            doubleDamageValue,
            !!lucky.property( 'checked' ),
            !painseeker.property( 'checked' ),
            shockChanceValue,
            shockDurationValue,
            amplifier,
            shockModValue,
            shockCap,
            thresholdValue
        );

        // render hit distribution
        const xRange = data.maxDamage - data.minDamage + 1;
        const xBuffer = Math.floor( xRange * 0.05 );
        const xLowerBound = data.minDamage - xBuffer;
        const yMax = Math.max( ...data.distribution.map(( hit ) => hit.probability ));
        x.domain([ 0 > xLowerBound? 0 : xLowerBound, data.maxDamage + xBuffer ])
            .range([ 0, w ]);
        y.domain([ 0, yMax * 1.1 ])
            .range([ h, 0 ]);
        xAxis.transition()
            .duration( 500 )
            .call( xAxisCall );
        yAxis.transition()
            .duration( 500 )
            .call( yAxisCall );
        lineCall.x( d => x( d.damage ))
            .y( d => y( d.probability ))
            .curve( d3.curveLinear );
        distLine.datum( data.distribution )
            .transition()
            .duration( 500 )
            .attr( 'd', lineCall );

        // render highest shock-applying hit (95% confidence)
        lineCall.x( d => x( data.c95damage ) )
            .y( d => y( d ) )
            .curve( d3.curveLinear );
        confidenceLine.datum([ 0, data.c95probability ])
            .transition()
            .duration( 500 )
            .attr( 'd', lineCall );
        confidenceLabel.classed( 'hide', false )
            .attr( 'transform', `translate(${ w * ( data.c95damage - xLowerBound ) / ( data.maxDamage - xLowerBound ) - ( Math.log10( data.c95damage )) * 12 },${ h - margin.bottom })` )
            .text( `${ Math.floor( data.c95damage ).toLocaleString() }` );

        // Render sustained shock level
        shockValueResult.text( `Sustainable Shock: ${ Math.floor(data.sustainedShock * 100) }%` );
        effectiveDps.classed( 'hide', false )
            .text( `Effective DPS: ${ Math.round( getAverageHit( minHitValue, maxHitValue, critValue, multiValue, doubleDamageValue, data.sustainedShock ) * accuracyValue * atkSpeedValue ).toLocaleString() }` );
        chartWrapper.classed( 'hide', false );

    });

    /* Description Management */

    const toggleDescription = d3.select( '#toggle-description' );
    const description = d3.select( '#description' );

    let showDescription = false;

    toggleDescription.on( 'click', () => {
        if (showDescription) {
            toggleDescription.html('?');
            description.style('right', '-360px');
        }
        else {
            toggleDescription.html('X');
            description.style('right', '-10px');
        }
        showDescription = !showDescription;
    });

    d3.select( '.year' ).html( new Date().getFullYear() );

});
