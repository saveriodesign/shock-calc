# Shock Calculator

Uses attack speed, critical strike chance, and the resulting
damage distribution to calculate 95% uptime shock magnitude.

[Site](http://saveriodesign.gitlab.io/shock-calc)